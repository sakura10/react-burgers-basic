import React, { Component } from 'react';
import Layout from './components/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          {/* ถ้าต้องการใส่ content ภายใต้ Layout จำเป็นต้อง ใส่ {props.children} ที่ Layout component */}
          
          <BurgerBuilder />
        </Layout>
      </div>
    );
  }
}

export default App;
