import React, { Component } from 'react';
import Aux from '../../hoc/Aus';

class BurgerBuilder extends Component {
    render() {
        return (
            <Aux>
                <div>Burger</div>
                <div>Burger Controls</div>
            </Aux>
        );
    }
}

export default BurgerBuilder;